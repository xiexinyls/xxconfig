# .bashrc

# User specific aliases and functions

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

#export TERM=xterm-256color
#export TERM=xterm-256color
#export TERMINFO=/T1/xxie/localsoft/lib/terminfo
#export TERMINFO=/usr/share/terminfo/x/xterm-256color

alias back='cd $OLDPWD'
alias b=back
alias c=clear

alias ls='ls --color=auto -h'
alias grep='grep --color=auto'
alias pylab='ipython --pylab'
alias pt='python'
alias tm='tmux'
alias upddp='export DISPLAY=$(tmux show-environment | grep "DISPLAY\=" | sed 's/DISPLAY=//g')'

unset SSH_ASKPASS


# Source Intel config
if [ -f /opt/intel/cce/10.1.015/bin/iccvars.sh ]; then
   . /opt/intel/cce/10.1.015/bin/iccvars.sh
fi
if [ -f /opt/intel/fce/10.1.012/bin/ifortvars.sh ]; then
   . /opt/intel/fce/10.1.012/bin/ifortvars.sh
fi

ulimit -s unlimited

#source $HOME/clmodel/env.gnu
source $HOME/clmodel/env.intel

#for NCL
export NCARG_ROOT=~/local/ncl

#for EDITOR
export EDITOR=vim


export LSOFT=$HOME/local

#for local PYTHON
#export LDFLAGS="-L/T1/xxie/local/lib"
#export CFLAGS="-I/T1/xxie/local/include"

export HDF5=/T1/xxie/local/hdf5-intel

export NCO=/T1/xxie/local/nco

export GCC5=/T1/xxie/local/gcc5
export GCC5LDPATH=$GCC5/lib:$GCC5/lib64

export LD_LIBRARY_PATH=$HDF5/lib:$GCC5LDPATH:$HOME/local/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$NCO/lib:$LD_LIBRARY_PATH
export PKG_CONFIG_PATH=$HOME/local/lib/pkgconfig

export MANPATH=$HOME/local/share/man:$MANPATH

export SPARK=$HOME/local/spark
export YASM=$HOME/local/yasm
export FFMPEG=$HOME/local/ffmpeg


#export PATH=$NCARG_ROOT/bin:$LSOFT/bin:$LSOFT/xxbin:$LSOFT/python27/bin:$NETCDF/bin:$PATH
export PATH=$NCARG_ROOT/bin:$LSOFT/bin:$LSOFT/xxbin:$LSOFT/python27/bin:$PATH
export PATH=$GCC5/bin:$LSOFT/grads/bin:$PATH
export PATH=$FFMPEG/bin:$YASM/bin:$NCO/bin:$SPARK/bin:$PATH


export ESMFMK=$HOME/local/esmf/lib/libO/Linux.intel.64.intelmpi.default/esmf.mk

export XXUTILS=$HOME/code/xx_utils/xx_utils.ncl



#for python
export BLAS=$HOME/local/lib/libfblas.a
export LAPACK=$HOME/local/lib/liblapack.a

export PYTHONPATH=$HOME/code/pythonlib:$PYTHONPATH
export PYTHONSTARTUP=~/local/pythonstartup.py

export WORKON_HOME=$HOME/.virtualenvs
source $HOME/local/python27/bin/virtualenvwrapper.sh


